'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Building', {
      BuildingId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      Name: {
        type: DataType.STRING
      },
      UnitCount: {
        type: DataType.INTEGER
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
  down: function(queryInterface, DataType) {
    return queryInterface.dropTable('Building');
  }
};
