'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        'UnitId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        ['UnitId'],{
            type: 'FOREIGN KEY',
            name: 'FK_ResidentPerson_Unit',
            references: {
                table: { tableName: 'Unit', 
                            schema: process.env.schema 
                    },
                field:'UnitId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "ResidentPerson"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_ResidentPerson_Unit', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        'UnitId', {transaction: t})
        );
    });
  }
};