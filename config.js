const dotenv = require('dotenv').config();
module.exports = 
{
  development: {
    username: process.env.username,
    password: process.env.password,
    database: process.env.database,
    host: process.env.host,
    dialect: process.env.dialect,
    pool: {
        max: 5,
        min: 0,
        idle: 20000
    }
  }
}