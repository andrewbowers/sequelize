'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      {
        tableName: 'PersonPhone',
        schema: process.env.schema
      },
      'PhoneId',
      Sequelize.UUID
    )
    .then(() =>
      queryInterface.addConstraint({
        tableName: 'PersonPhone',
        schema: process.env.schema
      },
      ['PhoneId'],{
        type: 'FOREIGN KEY',
        name: 'FK_PersonPhone_Phone',
        references: {
            table: { tableName: 'Phone', 
                        schema: process.env.schema 
                  },
            field:'PhoneId'
        }
      })
    );
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "PersonPhone"
    return queryInterface.removeConstraint(tableName,'FK_PersonPhone_Phone')
      .then(() =>
      queryInterface.removeColumn(
      {
        tableName: 'PersonPhone',
        schema: process.env.schema
      },
      'PhoneId')
    );
  }
};