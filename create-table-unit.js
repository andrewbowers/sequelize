'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Unit', {
      UnitId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      UnitType: {
        type: DataType.STRING
      },
      UnitBedrooms: {
        type: DataType.DECIMAL
      },
      UnitBathrooms: {
        type: DataType.DECIMAL
      },
      MinSquareFeet: {
        type: DataType.INTEGER
      },
      MaxSquareFeet: {
        type: DataType.INTEGER
      },
      UnitRent: {
        type: DataType.DECIMAL
      },
      MarketRent: {
        type: DataType.DECIMAL
      },
      UnitOccupancyStatusInfo: {
        type: DataType.STRING,
        allowNull: false
      },
      FloorPlan: {
        type: DataType.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, DataType) {
      return queryInterface.dropTable('Unit');
    }
  };
