'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('CompanyPerson', {
      CompanyPersonId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      CompanyRole: {
        type: DataType.STRING
      },
      CompanyTitle: {
        type: DataType.STRING
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('CompanyPerson');
  }
};
