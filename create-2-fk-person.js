'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      {
        tableName: 'Person',
        schema: process.env.schema
      },
      'AddressId',
      Sequelize.UUID
    )
    .then(() =>
      queryInterface.addConstraint({
        tableName: 'Person',
        schema: process.env.schema
      },
      ['AddressId'],{
        type: 'FOREIGN KEY',
        name: 'FK_Person_Address',
        references: {
            table: { tableName: 'Address', 
                        schema: process.env.schema 
                  },
            field:'AddressId'
        }
      })
    );
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Person"
    return queryInterface.removeConstraint(tableName,'FK_Person_Address')
      .then(() =>
      queryInterface.removeColumn(
      {
        tableName: 'Person',
        schema: process.env.schema
      },
      'AddressId')
    );
  }
};