'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Property', {
      PropertyId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      MarketingName: {
          type: DataType.STRING
      },
      Apn: {
          type: DataType.STRING
      },
      LegalName: {
          type: DataType.STRING
      },
      MSA_Number: {
          type: DataType.INTEGER
      },
      StructureDescription: {
          type: DataType.STRING
      },
      Website: {
          type: DataType.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, DataType) {
      return queryInterface.dropTable('Property');
    }
  };
