'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'Property',
            schema: process.env.schema
        },
        'CompanyId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'Property',
            schema: process.env.schema
        },
        ['CompanyId'],{
            type: 'FOREIGN KEY',
            name: 'FK_Property_Company',
            references: {
                table: { tableName: 'Company', 
                            schema: process.env.schema 
                    },
                field:'CompanyId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Property"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_Property_Company', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'Property',
            schema: process.env.schema
        },
        'CompanyId', {transaction: t})
        );
    });
  }
};