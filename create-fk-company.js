'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addConstraint('Company',{
        type: 'foreign key',
        name: 'FK_Company_Addresses',
        references: {
            table:'Addreesses',
            field:'AddressId'
        }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.removeConstraint('Company','FK_Company_Addresses');
  }
};