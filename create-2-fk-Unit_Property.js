'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'Unit',
            schema: process.env.schema
        },
        'PropertyId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'Unit',
            schema: process.env.schema
        },
        ['PropertyId'],{
            type: 'FOREIGN KEY',
            name: 'FK_Unit_Property',
            references: {
                table: { tableName: 'Property', 
                            schema: process.env.schema 
                    },
                field:'PropertyId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Unit"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_Unit_Property', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'Unit',
            schema: process.env.schema
        },
        'PropertyId', {transaction: t})
        );
    });
  }
};