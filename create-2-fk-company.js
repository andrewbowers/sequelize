'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.addColumn(
      {
        tableName: 'Company',
        schema: process.env.schema
      },
      'AddressId',
      Sequelize.UUID
    )
    .then(() =>
      queryInterface.addConstraint({
        tableName: 'Company',
        schema: process.env.schema
      },
      ['AddressId'],{
        type: 'FOREIGN KEY',
        name: 'FK_Company_Address',
        references: {
            table: { tableName: 'Address', 
                        schema: process.env.schema 
                  },
            field:'AddressId'
        }
      })
    );
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Company"
    return queryInterface.removeConstraint(tableName,'FK_Company_Address')
      .then(() =>
      queryInterface.removeColumn(
      {
        tableName: 'Company',
        schema: process.env.schema
      },
      'AddressId')
    );
  }
};