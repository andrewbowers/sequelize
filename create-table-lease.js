'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Lease', {
      LeaseId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      CurrentRent: {
        type: DataType.DECIMAL
      },
      ExpectedMoveInDate: {
        type: DataType.DATE
      },
      ExpectedMoveOutDate: {
        type: DataType.DATE
      },
      LeaseFromDate: {
        type: DataType.DATE
      },
      LeaseToDate: {
        type: DataType.DATE
      },
      ActualMoveIn: {
        type: DataType.DATE
      },
      ActualMoveOut: {
        type: DataType.DATE
      },
      LeaseSignDate: {
        type: DataType.STRING
      },
      PaymentAccepted: {
        type: DataType.STRING
      },
      LeaseStatus: {
        type: DataType.STRING
      },
      AccountNumber: {
        type: DataType.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, DataType) {
      return queryInterface.dropTable('Lease');
    }
  };
