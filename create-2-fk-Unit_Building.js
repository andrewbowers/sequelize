'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'Unit',
            schema: process.env.schema
        },
        'BuildingId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'Unit',
            schema: process.env.schema
        },
        ['BuildingId'],{
            type: 'FOREIGN KEY',
            name: 'FK_Unit_Building',
            references: {
                table: { tableName: 'Building', 
                            schema: process.env.schema 
                    },
                field:'BuildingId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Unit"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_Unit_Building', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'Unit',
            schema: process.env.schema
        },
        'BuildingId', {transaction: t})
        );
    });
  }
};