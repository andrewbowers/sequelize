'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('ResidentPerson', {
      ResidentPersonId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, DataType) {
      return queryInterface.dropTable('ResidentPerson');
    }
  };
