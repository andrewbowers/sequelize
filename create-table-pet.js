'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Pet', {
      PetId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      PetType: {
        type: DataType.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Pet');
  }
};
