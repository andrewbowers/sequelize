'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    var dest = process.env.schema + ".Company"
    return queryInterface.addIndex(dest, ['EmailAddress'], 
      {
        indexName: 'ix_company_emailaddress',
        indexType: 'BTREE',
      }
    );
  },
  down: function(queryInterface, Sequelize) {
    var dest = process.env.schema + ".Company"
    return queryInterface.removeIndex(dest,'ix_company_emailaddress',{ schema: process.env.schema });
  }
};
