'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        'PersonId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        ['PersonId'],{
            type: 'FOREIGN KEY',
            name: 'FK_ResidentPerson_Person',
            references: {
                table: { tableName: 'Person', 
                            schema: process.env.schema 
                    },
                field:'PersonId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "ResidentPerson"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_ResidentPerson_Person', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'ResidentPerson',
            schema: process.env.schema
        },
        'PersonId', {transaction: t})
        );
    });
  }
};