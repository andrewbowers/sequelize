'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.addColumn(
        {
            tableName: 'Property',
            schema: process.env.schema
        },
        'AddressId',
        Sequelize.UUID,
        {transaction: t})
        .then(() => {
        queryInterface.addConstraint({
            tableName: 'Property',
            schema: process.env.schema
        },
        ['AddressId'],{
            type: 'FOREIGN KEY',
            name: 'FK_Property_Address',
            references: {
                table: { tableName: 'Address', 
                            schema: process.env.schema 
                    },
                field:'AddressId'
            }
        }, {transaction: t});
        });
    })
  },
  down: function(queryInterface, Sequelize) {
    var tableName = process.env.schema + "." + "Property"
    return queryInterface.sequelize.transaction(t=> {
        return queryInterface.removeConstraint(tableName,'FK_Property_Address', {transaction: t})
        .then(() =>
        queryInterface.removeColumn(
        {
            tableName: 'Property',
            schema: process.env.schema
        },
        'AddressId', {transaction: t})
        );
    });
  }
};