'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Company', {
      CompanyId: {
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      Website: {
          type: Sequelize.STRING
      },
      Logo: {
          type: Sequelize.STRING
      },
      EmailAddress: {
          type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }    
    },
      {
        schema: 'AFBTest'
      }
    );
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Company');
  }
};
