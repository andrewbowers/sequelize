'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('Person', {
      PersonId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      Email: {
          type: DataType.STRING,
          allowNull: false
      },
      NamePrefix: {
          type: DataType.STRING
      },
      FirstName: {
          type: DataType.STRING,
          allowNull: false
      },
      MiddleName: {
          type: DataType.STRING
      },
      LastName: {
          type: DataType.STRING,
          allowNull: false
      },
      MaidenName: {
          type: DataType.STRING
      },
      NameSuffix: {
          type: DataType.STRING
      },
      Archived: {
        type: DataType.DATE
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Person');
  }
};
