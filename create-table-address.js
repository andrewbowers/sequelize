'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Address', {
      AddressId: {
        allowNull: false,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.UUID
      },
      Description: {
        type: Sequelize.STRING
      },
      AddressLine1: {
          type: Sequelize.STRING,
          allowNull: false
      },
      AddressLine2: {
          type: Sequelize.STRING
      },
      City: {
          type: Sequelize.STRING,
          allowNull: false
      },
      State: {
          type: Sequelize.STRING
      },
      Province: {
          type: Sequelize.STRING
      },
      PostalCode: {
          type: Sequelize.STRING,
          allowNull: false
      },
      Country: {
          type: Sequelize.STRING,
          allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Addresses');
  }
};
