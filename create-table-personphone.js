'use strict';
module.exports = {
  up: function(queryInterface, DataType) {
    return queryInterface.createTable('PersonPhone', {
      PhoneId: {
          type: DataType.UUID,
          defaultValue: DataType.UUIDV4,
          allowNull: false,
          primaryKey: true
      },
      PhoneType: {
          type: DataType.STRING,
          allowNull: false
      },
      PhoneNumber: {
          type: DataType.STRING,
          allowNull: false
      },
      PhoneDescription: {
          type: DataType.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataType.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataType.DATE
      }
    },
      {
        schema: 'AFBTest'
      }
    );
  },
    down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('PersonPhone');
  }
};
